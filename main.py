import csv
from datetime import datetime,date
import pandas as pd
import dateutil
import functools
import numpy
def get_date(t):
    return datetime.strptime(t[3], '%d.%m.%Y')

def birthday(persoana):
    # Get the current date
    date=get_date(persoana)
    now = datetime.utcnow()
    now = now.date()

    # Get the difference between the current date and the birthday
    age = dateutil.relativedelta.relativedelta(now, date)
    age = age.years

    return age
def compare_age(person1,person2):
    return (birthday(person1)<birthday(person2))



df=pd.read_csv("test1.csv",sep=',',header=None)
m= list(df.values)
indices=m[0]
t=m[1]

person_list=m.pop(0)
r=sorted(m, key=lambda person:birthday(person))
print r


with open("test2.csv","wb") as f:
    writer=csv.writer(f)
    writer.writerows(m)